class Code
  attr_accessor :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  PEGS = { red: "r", green: "g", blue: "b", yellow: "y", orange: "o", \
      purple: "p" }

  def self.parse(string)
    string.each_char do |ch|
      unless PEGS.values.include?(ch.downcase)
        raise_exception
      end
    end
    return Code.new(string.downcase.split(""))
  end

  def self.random
    return Code.new([PEGS.values.sample,PEGS.values.sample,\
      PEGS.values.sample,PEGS.values.sample])
  end

  def [](idx)
    @pegs[idx]
  end

  def []=(idx, value)
    @pegs[idx] = value
  end

  def exact_matches(arr)
    number = 0
    self.pegs.each_with_index do |el_s,i|
      arr.pegs.each_with_index do |el_a,j|
        if i != j
          next
        else
          if el_s == el_a
            number += 1
          end
        end
      end
    end
    return number
  end

  def near_matches(arr)
    number = 0
    arr.pegs.uniq.each do |el|
      if self.pegs.include?(el)
        number += 1
      end
    end
    return number - self.exact_matches(arr)
  end

  # def near_matches(arr)
  #   number = 0
  #   count_self = Hash.new(0)
  #   self.pegs.each { |el| count_self[el] += 1 }
  #   count_arr = Hash.new(0)
  #   arr.pegs.each { |el| count_arr[el] += 1 }
  #   count_self.each do |k,v|
  #     if v <= count_arr[k]
  #       number += count_arr[k]
  #     end
  #   end
  #   return number - self.exact_matches(arr)
  # end

  def ==(arr)
    if arr.is_a?(Code)
      if self.pegs == arr.pegs
        return true
      else
        return false
      end
    else
      return false
    end
  end

end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    puts "What's your guess?"
    input = gets.chomp
    @guess = Code.parse(input)
  end

  def display_matches(code)
    puts "You have #{secret_code.exact_matches(code)} exact matches\
    and #{secret_code.near_matches(code)} near matches."
  end

end
